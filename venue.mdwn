
Our wedding anniversary will be held at Sage Hill Campground, which is
a half hour from Santa Barbara and Solvang.

## Directions ##

From US 101, take CA 154 towards Cachuma Lake and turn onto Paradise
Road (from the south, a right turn after 11 miles; from the north, a
left turn after 30 miles). Once on Paradise Road, continue for 4.5
miles until you reach the Ranger Station on the left and a sign on the
right for Sage Hill Campground. Turn left onto this road. Continue
past the Ranger Station, and make a 180° right turn with the road when
possible. Head downhill, and follow the road left to the Santa Ynez
River crossing. This river crossing is paved, and can be crossed by
any passenger vehicle. Proceed slowly and safely across the creek. Our
venue will be on your right (at the Pine and Cactus campgrounds).

## Google Maps ##

You can also view the [location on google maps](https://www.google.com/maps/place/Sage+Hill+Campground/@34.545575,-119.789287,17z/data=!3m1!4b1!4m2!3m1!1s0x80e9427583a4282f:0x6554259505fcee4b?hl=en)

## Parking Passes ##

All cars at the venue must have an
[Adventure Pass](http://en.wikipedia.org/wiki/National_Forest_Adventure_Pass),
[Senior Pass](https://store.usgs.gov/pass/senior.html), Access Pass,
or an
[America The Beautiful Pass](http://store.usgs.gov/pass/index.html).
If you are over 65, the Senior Pass is easy to obtain; otherwise, a
daily or weekly Adventure Pass can be obtained from most sporting
goods stores. Please carpool if you can as there is limited parking.

[[!if test="enabled(sidebar)" then="""
[[!sidebar]]
""" else="""
[[!inline pages=sidebar raw=yes]]
"""]]
